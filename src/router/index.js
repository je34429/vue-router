import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Contact from '../views/Contact'
import About from '../views/About'
import Game from '../views/Game'
import E404 from '../views/E404'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact
  },
  {
    path: '/games/:name',
    component:Game
  },
  {
    path: '/:catchAll(.*)',
    component: E404
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
